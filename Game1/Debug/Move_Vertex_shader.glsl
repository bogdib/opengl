#version 450 core 
layout(location = 0) in vec3 in_position;
layout(location = 1) in vec2 in_texture;
 
out vec2 texcoord;
flat out int in_textNum;
uniform vec2 offset;
uniform int textNum;


void main(void)
{
	in_textNum = textNum;
	texcoord = in_texture;
	gl_Position = vec4(offset,0,0) + vec4(in_position, 1);
}