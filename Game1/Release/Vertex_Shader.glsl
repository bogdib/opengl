#version 450 core //lower this version if your card does not support GLSL 4.5
layout(location = 0) in vec3 in_position;
layout(location = 1) in vec2 in_texture;

out vec2 texcoord;
//uniform float xul;

void main(void)
{
	texcoord = in_texture;
  /* const vec4 vertices[4] = vec4[4](vec4( xul, -0.25, 0.5, 1.0),
                                    vec4(-0.25, -0.25, 0.5, 1.0),
                                    vec4( xul, 0.25, 0.5, 1.0),
									vec4( -0.25, 0.25, 0.5, 1.0));*/

   gl_Position = vec4(in_position, 1);
}