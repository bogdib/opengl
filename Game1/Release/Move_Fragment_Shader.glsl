#version 450 core
layout(location = 0) out vec4 out_color;
 
uniform sampler2D texturel1;
uniform sampler2D texturel2;

 
in vec2 texcoord;
flat in int in_textNum;
void main()
{
	vec4 color;
	if( in_textNum == 1)
	{
		color = texture(texturel1, texcoord);
	}
	else
	{
		color = texture(texturel2, texcoord);
	}

	out_color = color;

}