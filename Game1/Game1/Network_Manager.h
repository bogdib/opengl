#pragma once
#include <iostream>
#include "ServerGame.h"
#include "ClientGame.h"
#include <process.h>
//#include "Util.h"
//ServerGame * server;
//ClientGame * client;

enum ConnectionType
{

	INVALID = 0,
	SERVER,
	CLIENT,
};


class Network_Manager
{
public:
	ConnectionType connType;
	ServerGame *server;
	ClientGame *client;

	static float ox, oy, tNum;

	Network_Manager(char *type, ServerGame * server, ClientGame * client);
	Network_Manager() = default;

	void getData(float &ox, float &oy, float &tNum);
	void sendData(float ox, float oy, float tNum);
};


