#pragma once
#include "Shader_Manager.h"
#include "IListner.h"
#include "Models_Manager.h"
#include "Util.h"
#include "Network_Manager.h"
namespace Managers
{
	class Scene_Manager : public Core::IListener
	{
	public:
		Scene_Manager()=default;
		Scene_Manager(Network_Manager net);
		~Scene_Manager();

		virtual void notifyBeginFrame();
		virtual void notifyDisplayFrame();
		virtual void notifyEndFrame();
		virtual void notifyReshape(int width, int height, int previous_width, int previous_height);
	private:
		Managers::Shader_Manager* shader_manager;
		Managers::Models_Manager* models_manager;
	};
}