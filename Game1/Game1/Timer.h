#pragma once
#include <iostream>
#include <time.h>

class timer {
private:
	unsigned long begTime = 0;
public:
	void start() {
		begTime = clock();
	}

	unsigned long elapsedTime() {
		return ((unsigned long)clock() - begTime) / CLOCKS_PER_SEC;
	}

	bool isTimeout(unsigned long seconds) {
		return seconds >= elapsedTime();
	}
	bool isOn() 
	{
		return this->begTime==0?false:true;
	}
};