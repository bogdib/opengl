#pragma once
#include "Model.h"
#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds
#include <glm\gtc\type_ptr.hpp>
#include "Timer.h"
#include "Util.h"
#include "Network_Manager.h"
//#include "Keyboard_Manager.cpp"
namespace Rendering
{
	namespace Models
	{
		class QuadMove : public Model
		{
		public:
			static clock_t t1, t2;
			double offsetx = 0;
			double offsety = 0;
			bool jump = false;
			int timer = 0;
			//Animation anim;
			GLint textNum;
			Network_Manager net;


			QuadMove();
			~QuadMove();
			double xul; 
			double yul;
			double xlr;
			double ylr;
			Corners StaticObjList;

			void Create(double xul, double yul, double xlr, double ylr, Corners StaticObj);
			virtual void Draw() override final;
			virtual void Update() override final;
		};
		
	}
}