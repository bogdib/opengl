#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <vector>
enum Direction
{
	UP = 0,
	DOWN,
	LEFT,
	RIGHT,
};
enum Type
{
	NORMAL = 0,
	FINISH,
	DEATH
};

struct Animation
{

	char l1[1000], l2[1000], r1[1000], r2[1000], mid[1000];
	Animation() = default;
	Animation(const char *l1,const char *l2,const char *r1,const char *r2,const char *mid)
	{
		strcpy_s(this->l1, l1);
		strcpy_s(this->l2, l2);
		strcpy_s(this->r1, r1);
		strcpy_s(this->r2, r2);
		strcpy_s(this->mid, mid);
	}

};

struct Corners
{
	double xul, yul, xlr, ylr;
	Type type;
	std::vector<Corners> cornerList;
	Corners() = default;
	Corners(double a, double b, double c, double d,Type type)
	{
		this->xul = a;
		this->yul = b;
		this->xlr = c;
		this->ylr = d;
		this->type = type;
		//this->cornerList.push_back(*this);
	}

	void AddCorner(double a, double b, double c, double d , Type type = NORMAL)
	{
		//Corners corn(a, b, c, d);
		this->cornerList.push_back(Corners(a,b,c,d,type));
	}
public:
	double checkAxis(double curPoz,double kleft, double kright, Direction dir,Type &typeCheck)
	{
		kleft += 1;
		kright += 1;
		double minDist = 500.;
		for (int i = 0; i < this->cornerList.size(); i++)
		{
			double comp;
			double leftp, rightp;
			bool ok = false;
			switch (dir)
			{
			case UP:
				comp = this->cornerList[i].ylr;
				leftp = this->cornerList[i].xul + 1;
				rightp = this->cornerList[i].xlr + 1;
				if ((leftp < kleft&&kleft < rightp) || (leftp < kright && kright < rightp) ||
					(kleft < leftp && leftp < kright) || (kleft < rightp && rightp < kright) || (kleft == leftp) || (kright == rightp))
					ok = true;
				break;
			case DOWN:
				comp = this->cornerList[i].yul;
				//comp = this->cornerList[i].xlr;
				leftp = this->cornerList[i].xul + 1;
				rightp = this->cornerList[i].xlr + 1;
				if ((leftp < kleft&&kleft < rightp) || (leftp < kright && kright < rightp) ||
					(kleft < leftp && leftp < kright) || (kleft < rightp && rightp < kright)||(kleft == leftp)||(kright==rightp))
					ok = true;
				break;
			case LEFT:
				comp = this->cornerList[i].xlr;
				leftp = this->cornerList[i].yul + 1;
				rightp = this->cornerList[i].ylr + 1;

				if ((leftp > kleft&&kleft > rightp) || (leftp > kright && kright > rightp) ||
					(kleft > leftp && leftp > kright) || (kleft > rightp && rightp > kright))
					ok = true;
				break;
			case RIGHT:
				comp = this->cornerList[i].xul; 
				leftp = this->cornerList[i].yul + 1;
				rightp = this->cornerList[i].ylr + 1;
				if ((leftp > kleft&&kleft > rightp) || (leftp > kright && kright > rightp) ||
					(kleft > leftp && leftp > kright) || (kleft > rightp && rightp > kright))
					ok = true;
				break;
			default:
				break;
			}
			/*if (abs(limit - comp) < minDist)
			{
				minDist = abs(limit - comp);
			}*/
			if (!ok)
				continue;
			if (abs(curPoz - comp) < minDist && ((curPoz - comp > 0 && dir != UP) || (curPoz - comp < 0 && dir == UP)))
			{
				minDist = abs(curPoz - comp);
				typeCheck = cornerList[i].type;
			}
		}
		return minDist;
	}
};