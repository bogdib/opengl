#include "Models_Manager.h"

using namespace Managers;
using namespace Rendering;

Models_Manager::Models_Manager()
{
}



Models_Manager::~Models_Manager()
{
	//auto -it's a map iterator
	for (auto model : gameModelList)
	{
		delete model.second;
	}
	gameModelList.clear();
}

void Models_Manager::DeleteModel(const std::string& gameModelName)
{
	IGameObject* model = gameModelList[gameModelName];
	model->Destroy();
	gameModelList.erase(gameModelName);
}

const IGameObject& Models_Manager::GetModel(const std::string& gameModelName) const
{
	return (*gameModelList.at(gameModelName));
}

void Managers::Models_Manager::AddTriangle()
{
	Models::Triangle* triangle = new Models::Triangle();
	triangle->SetProgram(Shader_Manager::GetShader("colorShader"));
	triangle->Create();
	gameModelList["triangle"] = triangle;
}

void Managers::Models_Manager::AddQuad(double xul, double yul, double xlr, double ylr,int id, char *texturePath )
{
	Models::Quad* quad = new Models::Quad();
	quad->SetProgram(Shader_Manager::GetShader("staticShader"));
	quad->Create(xul,yul,xlr,ylr);
	unsigned int texture = Rendering::Texture::TextureLoader::LoadTexture(texturePath, 256, 256);
	char s[10];
	sprintf_s(s, "%d", id);
	quad->SetTexture("Create",texture);
	gameModelList[s] = quad;
}
void Managers::Models_Manager::AddQuadMove(double xul, double yul, double xlr, double ylr, int id, Animation animation/*char *texturePath*/, Corners cornerList,Network_Manager net)//,std::vector<Corners> StaticObj)
{
	Models::QuadMove* quad = new Models::QuadMove();
	quad->SetProgram(Shader_Manager::GetShader("moveShader"));
	quad->Create(xul, yul, xlr, ylr,cornerList);
	unsigned int texturel1 = Rendering::Texture::TextureLoader::LoadTexture(animation.l1, 256, 256);
	unsigned int texturel2 = Rendering::Texture::TextureLoader::LoadTexture(animation.l2, 256, 256);
	unsigned int texturemid = Rendering::Texture::TextureLoader::LoadTexture(animation.mid, 256, 256);
	unsigned int texturer1 = Rendering::Texture::TextureLoader::LoadTexture(animation.r1, 256, 256);
	unsigned int texturer2 = Rendering::Texture::TextureLoader::LoadTexture(animation.r2, 256, 256);


	quad->SetTexture("l1", texturel1);
	quad->SetTexture("l2", texturel2);
	quad->SetTexture("mid", texturemid);
	quad->SetTexture("r1", texturer1);
	quad->SetTexture("r2", texturer2);
	//strcpy_s(quad->curentTexture, "l1");
	//quad->anim = animation;
	quad->textNum = 3;
	quad->net = net;
	char s[10];
	sprintf_s(s, "%d", id);
	gameModelList[s] = quad;
}

void Managers::Models_Manager::AddQuadNetwork(double xul, double yul, double xlr, double ylr, int id, Animation animation, Corners cornerList,Network_Manager net)
{
	Rendering::Models::QuadNetwork* quad = new Models::QuadNetwork();
	quad->SetProgram(Shader_Manager::GetShader("moveShader"));
	quad->Create(xul, yul, xlr, ylr, cornerList,net);
	unsigned int texturel1 = Rendering::Texture::TextureLoader::LoadTexture(animation.l1, 256, 256);
	unsigned int texturel2 = Rendering::Texture::TextureLoader::LoadTexture(animation.l2, 256, 256);
	unsigned int texturemid = Rendering::Texture::TextureLoader::LoadTexture(animation.mid, 256, 256);
	unsigned int texturer1 = Rendering::Texture::TextureLoader::LoadTexture(animation.r1, 256, 256);
	unsigned int texturer2 = Rendering::Texture::TextureLoader::LoadTexture(animation.r2, 256, 256);


	quad->SetTexture("l1", texturel1);
	quad->SetTexture("l2", texturel2);
	quad->SetTexture("mid", texturemid);
	quad->SetTexture("r1", texturer1);
	quad->SetTexture("r2", texturer2);
	//strcpy_s(quad->curentTexture, "l1");
	//quad->anim = animation;
	quad->textNum = 3;
	char s[10];
	sprintf_s(s, "%d", id);
	gameModelList[s] = quad;

}

void Models_Manager::Update()
{
	for (auto model : gameModelList)
	{
		model.second->Update();
	}
}

void Models_Manager::Draw()
{
	//auto -it's a map iterator
	for (auto model : gameModelList)
	{
		model.second->Draw();
	}
}