#pragma once
#include "Model.h"
#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds
#include <glm\gtc\type_ptr.hpp>
#include "Timer.h"
#include "Util.h"
#include "Network_Manager.h"
//#include "Keyboard_Manager.cpp"
namespace Rendering
{
	namespace Models
	{
		class QuadNetwork : public Model
		{
		public:
			double offsetx = 0;
			double offsety = 0;
			//bool jump = false;
			//char curentTexture[1000];
			//Animation anim;
			GLint textNum;
			Network_Manager net;

			QuadNetwork();
			~QuadNetwork();
			double xul;
			double yul;
			double xlr;
			double ylr;
			Corners StaticObjList;

			void Create(double xul, double yul, double xlr, double ylr, Corners StaticObj,Network_Manager net);
			virtual void Draw() override final;
			virtual void Update() override final;
		};
	}
}