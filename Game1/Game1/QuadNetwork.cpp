#include "QuadNetwork.h"
using namespace Rendering;
using namespace Models;

QuadNetwork::QuadNetwork()
{}

QuadNetwork::~QuadNetwork()
{}


void QuadNetwork::Create(double xul, double yul, double xlr, double ylr, Corners StaticObj, Network_Manager net)
{
	this->StaticObjList = StaticObj;
	this->xul = xul;
	this->xlr = xlr;
	this->yul = yul;
	this->ylr = ylr;
	this->net = net;

	GLuint vao;
	GLuint vbo;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	std::vector<VertexFormat> vertices;
	vertices.push_back(VertexFormat(glm::vec3(xlr, ylr, 0.0), //1 ul
		glm::vec2(1, 0)));
	vertices.push_back(VertexFormat(glm::vec3(xul, ylr, 0.0), //2 ur
		glm::vec2(0, 0)));
	vertices.push_back(VertexFormat(glm::vec3(xlr, yul, 0.0), //3 ll
		glm::vec2(1, 1)));
	vertices.push_back(VertexFormat(glm::vec3(xul, yul, 0.0), //4 lr
		glm::vec2(0, 1)));

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);            //here we have 4
	glBufferData(GL_ARRAY_BUFFER, sizeof(VertexFormat) * 4, &vertices[0], GL_DYNAMIC_DRAW);
	//glClearColor(1, 0, 1, 1);
	//glm::uint16 vertexLoc = glGetAttribLocation(program, "a_position");
	//glVertexAttribPointer(glGetAttribLocation(program, "a_position"), 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)0);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
		sizeof(VertexFormat),
		(void*)(offsetof(VertexFormat, VertexFormat::texture)));
	glBindVertexArray(0);



	this->vao = vao;
	this->vbos.push_back(vbo);


}

void QuadNetwork::Update()
{
	float a, b, c=-1;
	net.getData(a,b,c);
	if (c != -1)
	{
		this->offsetx = a;
		this->offsety = b;
		this->textNum = c;
	}
}

void QuadNetwork::Draw()
{

	glUseProgram(program);
	glBindVertexArray(vao);

	unsigned int texLoc;


	texLoc = glGetUniformLocation(program, "texturel1");
	glUniform1i(texLoc, 0);
	texLoc = glGetUniformLocation(program, "texturel2");
	glUniform1i(texLoc, 1);
	texLoc = glGetUniformLocation(program, "texturemid");
	glUniform1i(texLoc, 2);
	texLoc = glGetUniformLocation(program, "texturer1");
	glUniform1i(texLoc, 3);
	texLoc = glGetUniformLocation(program, "texturer2");
	glUniform1i(texLoc, 4);


	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, this->GetTexture("l1"));
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, this->GetTexture("l2"));
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, this->GetTexture("mid"));
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, this->GetTexture("r1"));
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, this->GetTexture("r2"));

	glUniform1i(glGetUniformLocation(program, "textNum"), textNum);
	glUniform2f(glGetUniformLocation(program, "offset"), this->offsetx, this->offsety);


	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

