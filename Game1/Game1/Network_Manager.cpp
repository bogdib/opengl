#include "Network_Manager.h"


Network_Manager::Network_Manager(char *type, ServerGame * server, ClientGame * client)
{
	//strcpy_s(this->connType, type);
	
	if (!strcmp(type, "client"))
	{
		this->client = client;
		this->server = nullptr;
		this->connType = CLIENT;
	}
	else if (!strcmp(type, "server"))
	{
		this->server = server;
		this->client = nullptr;
		this->connType = SERVER;
	}
	else
	{
		std::cout << "Invalid connection type\n";
		this->connType = INVALID;
	}

}

void Network_Manager::getData(float & ox, float & oy, float & tNum)
{
	switch (this->connType)
	{
	case SERVER:
		ox = this->server->ox; oy = this->server->oy; tNum = this->server->tNum; break;
	case CLIENT:
		ox = this->client->ox; oy = this->client->oy; tNum = this->client->tNum; break;
	}

}

void Network_Manager::sendData(float ox, float oy, float tNum)
{
	switch (this->connType)
	{
	case SERVER:
		this->server->sendActionPackets(ox, oy, tNum); break;
	case CLIENT:
		this->client->sendActionPackets(ox, oy, tNum); break;
	}
}

