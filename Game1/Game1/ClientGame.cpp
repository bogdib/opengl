//#include "stdafx.h"
#include "ClientGame.h"

ClientGame::ClientGame(void)
{

	network = new ClientNetwork();

	// send init packet
	const unsigned int packet_size = 3 * sizeof(float);
	char packet_data[packet_size];

	//Packet packet;
	//packet.packet_type = INIT_CONNECTION;

	//packet.serialize(packet_data,0.0,0.0,0.0);

	//NetworkServices::sendMessage(network->ConnectSocket, packet_data, packet_size);
}

void ClientGame::sendActionPackets(float ox, float oy, float tNum)
{
	// send action packet
	const unsigned int packet_size = 3 * sizeof(float);
	char packet_data[packet_size];

	Packet packet;
	packet.packet_type = ACTION_EVENT;

	packet.serialize(packet_data, ox, oy, tNum);

	NetworkServices::sendMessage(network->ConnectSocket, packet_data, packet_size);
}
void ClientGame::update()
{
	Packet packet;
	int data_length = network->receivePackets(network_data);

	if (data_length <= 0)
	{
		//no data recieved
		return;
	}
	float ox, oy, tNum;
	//int i = 0;
	//while (true)
	//{
		packet.deserialize(&(network_data[0]),ox,oy,tNum);
		//i += 3 * sizeof(float);

		switch (packet.packet_type) {

		case ACTION_EVENT:
			//printf("client received %f %f %f from server\n", ox, oy, tNum);
			this->ox = ox;
			this->oy = oy;
			this->tNum = tNum;
			//Network_Manager::ox = ox;
			//Network_Manager::oy = oy;
			//Network_Manager::tNum = tNum;
			//sendActionPackets();
			break;

		default:
			printf("error in packet types\n");
			break;
		}
	//}
}