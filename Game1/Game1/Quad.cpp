#include "Quad.h"
using namespace Rendering;
using namespace Models;

Quad::Quad()
{}

Quad::~Quad()
{}

void Quad::Create(double xul, double yul, double xlr, double ylr )
{
	GLuint vao;
	GLuint vbo;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	std::vector<VertexFormat> vertices;
	vertices.push_back(VertexFormat(glm::vec3(xlr, ylr, 0.0), //1 ul
		glm::vec2(0, 0)));
	vertices.push_back(VertexFormat(glm::vec3(xul, ylr, 0.0), //2 ur
		glm::vec2(1, 0)));
	vertices.push_back(VertexFormat(glm::vec3(xlr, yul, 0.0), //3 ll
		glm::vec2(0, 1)));
	vertices.push_back(VertexFormat(glm::vec3(xul, yul, 0.0), //4 lr
		glm::vec2(1, 1)));
	
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);            //here we have 4
	glBufferData(GL_ARRAY_BUFFER, sizeof(VertexFormat) * 4, &vertices[0], GL_STATIC_DRAW);
	



	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
		sizeof(VertexFormat),
		(void*)(offsetof(VertexFormat, VertexFormat::texture)));
	glBindVertexArray(0);
	
	
	this->vao = vao;
	this->vbos.push_back(vbo);

}

void Quad::Update()
{

}

void Quad::Draw()
{
	glUseProgram(program);
	glBindVertexArray(vao);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, this->GetTexture("Create"));
	unsigned int textureLocation = glGetUniformLocation(program, "texture1");
	glUniform1i(textureLocation, 0);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}