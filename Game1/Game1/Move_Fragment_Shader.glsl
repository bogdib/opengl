#version 450 core
layout(location = 0) out vec4 out_color;
 
uniform sampler2D texturel1;
uniform sampler2D texturel2;
uniform sampler2D texturemid;
uniform sampler2D texturer1;
uniform sampler2D texturer2;

 
in vec2 texcoord;
flat in int in_textNum;
void main()
{
	vec4 color;
	switch (in_textNum)
	{
	case 1:color = texture(texturel1, texcoord);break;
	case 2:color = texture(texturel2, texcoord);break;
	case 3:color = texture(texturemid, texcoord);break;
	case 4:color = texture(texturer1, texcoord);break;
	case 5:color = texture(texturer2, texcoord);break;
	}
	out_color = color;

}