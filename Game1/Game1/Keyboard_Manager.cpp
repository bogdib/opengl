#pragma once
#include <iostream>
#include <GL\freeglut.h>

double value;
static void processNormalKeys(unsigned char key, int x, int y)
{
	if (key == 'w')
	{
		value += 0.1;
	}
}
void runKeyboard(double &i)
{
	value = i;
	glutKeyboardFunc(processNormalKeys);
	i = value;
}
