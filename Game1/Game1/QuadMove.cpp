#include "QuadMove.h"
using namespace Rendering;
using namespace Models;

QuadMove::QuadMove()
{
}
clock_t QuadMove::t1 = 0, QuadMove::t2 = 0;
QuadMove::~QuadMove()
{}

bool up = false;
bool down = false;
bool kleft = false;
bool kright = false;
int vx = 0;
int vy = 0;


void displayText(float x, float y, int r, int g, int b, const char *string)
{
	//glMatrixMode(GL_PROJECTION);
	//glPushMatrix();
	//glLoadIdentity();
	//gluOrtho2D(0, 800, 0, 600);

	//glMatrixMode(GL_MODELVIEW);

	int j = strlen(string);

	glColor3f(0.9, 0., 0.);
	glRasterPos2f(20, 30 );
	//glTranslatef(20, 30, 0);
	for (int i = 0; i < j; i++) 
	{
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, string[i]);
	}

}


void keyPress(unsigned char key, int x, int y)
{
	if (key == 'w')
	{
		up = true;
	}
	else if (key == 's')
	{
		down = true;
	}
	else if (key == 'd')
	{
		kright = true;
	}
	else if (key == 'a')
	{
		kleft = true;
	}
	
}
void keyRelease(unsigned char key, int x, int y)
{
	if (key == 'w')
	{
		up = false;
	}
	else if (key == 's')
	{
		down = false;
	}
	else if (key == 'd')
	{
		kright = false;
	}
	else if (key == 'a')
	{
		kleft = false;
	}

}
void runKeyboard()
{
	glutKeyboardFunc(keyPress);
	glutKeyboardUpFunc(keyRelease);

}


void QuadMove::Create(double xul, double yul, double xlr, double ylr, Corners StaticObj)
{
	this->StaticObjList = StaticObj;
	this->xul = xul;
	this->xlr = xlr;
	this->yul = yul;
	this->ylr = ylr;

	GLuint vao;
	GLuint vbo;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	std::vector<VertexFormat> vertices;
	vertices.push_back(VertexFormat(glm::vec3(xlr, ylr, 0.0), //1 ul
		glm::vec2(1, 0)));
	vertices.push_back(VertexFormat(glm::vec3(xul, ylr, 0.0), //2 ur
		glm::vec2(0, 0)));
	vertices.push_back(VertexFormat(glm::vec3(xlr, yul, 0.0), //3 ll
		glm::vec2(1, 1)));
	vertices.push_back(VertexFormat(glm::vec3(xul, yul, 0.0), //4 lr
		glm::vec2(0, 1)));

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);            //here we have 4
	glBufferData(GL_ARRAY_BUFFER, sizeof(VertexFormat) * 4, &vertices[0], GL_DYNAMIC_DRAW);
	//glClearColor(1, 0, 1, 1);
	//glm::uint16 vertexLoc = glGetAttribLocation(program, "a_position");
	//glVertexAttribPointer(glGetAttribLocation(program, "a_position"), 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)0);
	
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
		sizeof(VertexFormat),
		(void*)(offsetof(VertexFormat, VertexFormat::texture)));
	glBindVertexArray(0);



	this->vao = vao;
	this->vbos.push_back(vbo);


}

void QuadMove::Update()
{
	if (t1 == 0)
	{
		t1 = t2 = clock();
	}
	else
	{
		t1 = t2;
		t2 = clock();
	}

	/*
	if (this->offsety > -0.75 && up == false)
		this->offsety -= 0.001;*/
	//timer t;
	//const int jumpTimeout = 10;
	double minDist;
	Type curType;
	this->timer++;
	// nu cu timer, sare, dezactivez buton, si cand detecteaza coliziune jos, il reactivez

	//jump = false;

	if (this->timer % 3 && net.connType != INVALID)
	{
		net.sendData(this->offsetx, this->offsety, this->textNum);
	}


	runKeyboard();

	if ((vx > -3 && vx < 3) && (vy > -3 && vy < 3))
		this->textNum = 3;

	//if (strcmp(this->curentTexture, "l1"))
	//	strcpy_s(this->curentTexture, "l2");
	//else
	//	strcpy_s(this->curentTexture, "l1");

	if (/*down == true*/ vy < 10)
	{
		minDist = this->StaticObjList.checkAxis(this->ylr + this->offsety, this->xul + this->offsetx, this->xlr + this->offsetx, DOWN, curType);
		if (minDist < 0.005 + (t2 - t1) / 1500.)
		{
			vy = 0;
			jump = true;
			if ((curType == 1 || curType == 2) )
			{
				this->offsetx = 0;
				this->offsety = 0;
				displayText(10, 200, 200, 0, 0, "AFISARE");

			}
		}
		else
		{
			vy = -20;
		
		}
	}

	if (up == true && jump/*&& ( t.elapsedTime()>jumpTimeout || t.isOn()==false)*/)
	{
		//t.start();
		//vy = 80;
		//up = false;
		//jump = false;
		minDist = this->StaticObjList.checkAxis(this->yul + this->offsety, this->xul + this->offsetx, this->xlr + this->offsetx, UP, curType);
		if (abs(minDist) < 0.5)
			vy = (abs(minDist) / ((t2 - t1) / 2000.))+ 0.08 / ((t2 - t1) / 2000.);
		else
			vy = 60;

		up = false;
		jump = false;
	}

	if (kleft == true)
	{
		minDist = this->StaticObjList.checkAxis(this->xul + this->offsetx,this->yul + this->offsety, this->ylr + this->offsety, LEFT, curType);
		if (minDist < 0.005)
			vx = 0;
		else
			vx = -20;
		//else vx = -1 * minDist;
	}
	if (kright == true)
	{
		minDist = this->StaticObjList.checkAxis(this->xlr + this->offsetx, this->yul + this->offsety, this->ylr + this->offsety, RIGHT, curType);
		if (minDist < 0.005)
			vx = 0;
		else
			vx = +20;
	}


	if(vx!=0 && vx>0 )
	{
		//this->offsetx += 0.0025;
		this->offsetx += (t2-t1)/4500.;
		vx -= 1;
		if (this->textNum == 4)
			this->textNum = 5;
		else
			this->textNum = 4;
	}
	if (vx != 0 && vx<0)
	{
		//this->offsetx -= 0.0025;
		this->offsetx -=  ((t2 - t1) / 4500.);
		vx += 1;
		if (this->textNum == 1)
			this->textNum = 2;
		else
			this->textNum = 1;
	}

	//UP
	if (vy != 0 && vy>0)
	{
		//this->offsety += 0.005; //10000
		this->offsety += (t2 - t1) / 2000.;

		vy -= 1;
		if (vy == 1)
			jump = false;
	}
	//DOWN
	if (vy != 0 && vy<0)
	{
		//this->offsety -= 0.004;//8000
		this->offsety -= (t2 - t1) / 1500.;
		vy += 1;
	}
	

	//std::cout << this->offsety;
	//std::cout << t.elapsedTime()<< std::endl;
	//runKeyboard(this->offsety);

	//this->offsety += 0.1;
	//std::cout << offsetx;
	//std::this_thread::sleep_for(std::chrono::seconds(2));


}

void QuadMove::Draw()
{
	//double xul=0.25, yul= -0.25, xlr=-0.25, ylr=0.25;
	//glm::vec3 vertices[4];
	//
	//vertices[0] = glm::vec4(xul, yul, 0.5f, 1.0f);
	//vertices[1] = glm::vec4(xlr, yul, 0.5f, 1.0f);
	//vertices[2] = glm::vec4(xul, ylr, 0.5f, 1.0f);
	//vertices[3] = glm::vec4(xlr, ylr, 0.5f, 1.0f);

	//glMatrixMode(GL_PROJECTION);
	
	glUseProgram(program);
	glBindVertexArray(vao);

	//glActiveTexture(GL_TEXTURE0);
	//glBindTexture(GL_TEXTURE_2D, this->GetTexture("l1"));
	//unsigned int textureLocation = glGetUniformLocation(program, "texturel1");
	//glUniform1i(textureLocation, 0);


	unsigned int texLoc;


	texLoc = glGetUniformLocation(program, "texturel1");
	glUniform1i(texLoc, 0);
	texLoc = glGetUniformLocation(program, "texturel2");
	glUniform1i(texLoc, 1);
	texLoc = glGetUniformLocation(program, "texturemid");
	glUniform1i(texLoc, 2);
	texLoc = glGetUniformLocation(program, "texturer1");
	glUniform1i(texLoc, 3);
	texLoc = glGetUniformLocation(program, "texturer2");
	glUniform1i(texLoc, 4);


	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, this->GetTexture("l1"));
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, this->GetTexture("l2"));
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, this->GetTexture("mid"));
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, this->GetTexture("r1"));
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, this->GetTexture("r2"));

	//GLint textNum;
	//textNum = 1;
	glUniform1i(glGetUniformLocation(program, "textNum"), textNum);


	glUniform2f(glGetUniformLocation(program, "offset"), (GLfloat)this->offsetx, (GLfloat)this->offsety );

	//glUniform4fv(glGetUniformLocation(program, "pos"), 4, glm::value_ptr(vertices[0]));
	/*glUniform1f(glGetUniformLocation(program, "xul"), xul);
	glUniform1f(glGetUniformLocation(program, "yul"), yul);
	glUniform1f(glGetUniformLocation(program, "xlr"), xlr);
	glUniform1f(glGetUniformLocation(program, "ylr"), ylr);*/


	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	displayText(10, 200, 1., 0., 0., "AFISARE");

}

