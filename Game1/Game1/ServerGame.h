#pragma once
#include "ServerNetwork.h"
#include "NetworkData.h"
//#include "Network_Manager.h"


class ServerGame
{

public:

	float ox, oy, tNum=-1;
	void receiveFromClients();
	void sendActionPackets(float ox, float oy, float tNum);
	ServerGame(void);
	~ServerGame(void);

	void update();

private:
	char network_data[MAX_PACKET_SIZE];
	// IDs for the clients connecting for table in ServerNetwork 
	static unsigned int client_id;

	// The ServerNetwork object 
	ServerNetwork* network;
};
