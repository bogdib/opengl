#pragma once
#include <winsock2.h>
#include <Windows.h>
#include "ClientNetwork.h"
#include "NetworkData.h"
//#include "Network_Manager.h"


class ClientGame
{

public:

	float ox, oy, tNum = -1;
	void sendActionPackets(float ox, float oy, float tNum);

	char network_data[MAX_PACKET_SIZE];

	void update();

	ClientGame();
	~ClientGame(void);

	ClientNetwork* network;
};