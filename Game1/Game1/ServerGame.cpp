//#include "stdafx.h"
#include "ServerGame.h"
//#include "stdafx.h" 
#include "ServerGame.h"

#include<iostream>

unsigned int ServerGame::client_id;

ServerGame::ServerGame(void)
{
	// id's to assign clients for our table
	client_id = 0;

	// set up the server network to listen 
	network = new ServerNetwork();
}


void ServerGame::receiveFromClients()
{

	Packet packet;

	// go through all clients
	std::map<unsigned int, SOCKET>::iterator iter;

	for (iter = network->sessions.begin(); iter != network->sessions.end(); iter++)
	{
		int data_length = network->receiveData(iter->first, network_data);

		if (data_length <= 0)
		{
			//no data recieved
			continue;
		}
		float ox, oy, tNum;
		//int i = 0;
		//while (true)//i < (unsigned int)data_length
		//{
			packet.deserialize(&(network_data[0]), ox, oy, tNum);
			//i += 3 * sizeof(float);
			
			switch (packet.packet_type)
 {
			case INIT_CONNECTION:
				printf("server received init packet from client\n");
				//sendActionPackets();
				break;
			case ACTION_EVENT:
				//printf("server received %f %f %f from client\n", ox, oy, tNum);
				this->ox = ox;
				this->oy = oy;
				this->tNum = tNum;
				/*Data::gox = ox;*/
				//Data::oy = oy;
				//Data::tNum = tNum;
		/*		Network_Manager::ox = ox;
				Network_Manager::oy = oy;
				Network_Manager::tNum = tNum;*/
				//sendActionPackets();
				break;
			default:
				printf("error in packet types\n");
				break;
			//}
		}
	}
}
void ServerGame::sendActionPackets(float ox, float oy, float tNum)
{
	// send action packet
	const unsigned int packet_size = 3 * sizeof(float);
	char packet_data[packet_size];

	Packet packet;
	packet.packet_type = ACTION_EVENT;

	packet.serialize(packet_data, ox, oy, tNum);

	network->sendToAll(packet_data, packet_size);
}

void ServerGame::update()
{
	// get new clients
	if (network->acceptNewClient(client_id))
	{
		printf("client %d has been connected to the server\n", client_id);
		//std::cout << "start";
		client_id++;
	}

	receiveFromClients();
}