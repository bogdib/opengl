#pragma once
#include <map>
#include "Shader_Manager.h"
#include "IGameObject.h"
#include "Triangle.h"
#include "Quad.h"
#include "QuadMove.h"
#include "QuadNetwork.h"
#include "TextureLoader.h"
#include "Util.h"

using namespace Rendering;
namespace Managers
{
	class Models_Manager
	{
	public:
		Models_Manager();
		~Models_Manager();

		void Draw();
		void Update();
		void DeleteModel(const std::string& gameModelName);
		const IGameObject& GetModel(const std::string& gameModelName) const;
		void AddTriangle();
		void AddQuad(double xul, double yul, double xlr, double ylr,int id, char *texturePath);

		void AddQuadMove(double xul, double yul, double xlr, double ylr, int id, Animation animation/*char * texturePath*/, Corners cornerList, Network_Manager net);//std::vector<Corners> StaticObj);
		void AddQuadNetwork(double xul, double yul, double xlr, double ylr, int id, Animation animation, Corners cornerList,Network_Manager net);

	private:
		std::map<std::string, IGameObject*> gameModelList;
	};
}