#include "Scene_Manager.h"

using namespace Managers;

Scene_Manager::Scene_Manager(Network_Manager net)
{
	glEnable(GL_DEPTH_TEST);

	shader_manager = new Shader_Manager();
	shader_manager->CreateProgram("staticShader",
		"Vertex_Shader.glsl",
		"Fragment_Shader.glsl");
	shader_manager->CreateProgram("moveShader",
		"Move_Vertex_Shader.glsl",
		"Move_Fragment_Shader.glsl");

	models_manager = new Models_Manager();

	//--------  Laptop Config

	//int id = 0;
	//char path1[1000] = "D:\\Scoala an 3\\Game\\ground.bmp";
	//char path2[1000] = "D:\\Scoala an 3\\Game\\png.bmp";
	//char path3[1000] = "D:\\Scoala an 3\\Game\\ground3.bmp";

	//char pathFinish[1000] = "D:\\Scoala an 3\\Game\\finish.bmp";
	//char pathDeath[1000] = "D:\\Scoala an 3\\Game\\death.bmp";

	//Animation anim("D:\\Scoala an 3\\Game\\animation\\st1.bmp","D:\\Scoala an 3\\Game\\animation\\st2.bmp",
	//	"D:\\Scoala an 3\\Game\\animation\\dr1.bmp", "D:\\Scoala an 3\\Game\\animation\\dr2.bmp",
	//	"D:\\Scoala an 3\\Game\\animation\\mid.bmp");

	//--------- Home Config
	int id = 0;
	char path1[1000] = "D:\\projects\\Game1\\repo_game\\Game1\\ground.bmp";
	char path2[1000] = "D:\\projects\\Game1\\repo_game\\Game1\\png.bmp";
	char path3[1000] = "D:\\projects\\Game1\\repo_game\\Game1\\ground3.bmp";

	char pathFinish[1000] = "D:\\projects\\Game1\\repo_game\\Game1\\finish.bmp";
	char pathDeath[1000] = "D:\\projects\\Game1\\repo_game\\Game1\\death.bmp";

	Animation anim("D:\\projects\\Game1\\repo_game\\Game1\\animation\\st1.bmp", "D:\\projects\\Game1\\repo_game\\Game1\\animation\\st2.bmp",
	"D:\\projects\\Game1\\repo_game\\Game1\\animation\\dr1.bmp", "D:\\projects\\Game1\\repo_game\\Game1\\animation\\dr2.bmp",
	"D:\\projects\\Game1\\repo_game\\Game1\\animation\\mid.bmp");


	Corners cornerList;
	std::vector<Corners> staticObj;
	
//--------Singel Player
	if (net.connType == INVALID)
	{

		for (double i = -1.; i <= -0.3; i += 0.1)
		{
			models_manager->AddQuad(i, -0.9, i + 0.1,-1.,id,path1);
			//staticObj.push_back(Corners(i , -0.9, i + 0.1, -1.));
			cornerList.AddCorner(i, -0.9, i + 0.1, -1.);
			id++;
		}
		for (double i = -0.3; i <= 0.9; i += 0.1)
		{
			models_manager->AddQuad(i, -0.9, i + 0.1, -1., id, path3);
			//staticObj.push_back(Corners(i , -0.9, i + 0.1, -1.));
			cornerList.AddCorner(i, -0.9, i + 0.1, -1.);
			id++;
		}
		
		
		//----
		for (double i = -0.2; i <= 0.3; i += 0.1)
		{
			models_manager->AddQuad(i, -0.7, i + 0.1, -0.8, id, path1);
			cornerList.AddCorner(i, -0.7, i + 0.1, -0.8);
			id++;
		}
		
		for (double i = 0.5; i <= 1.; i += 0.1)
		{
			models_manager->AddQuad(i, -0.7, i + 0.1, -0.8, id, path1);
			cornerList.AddCorner(i, -0.7, i + 0.1, -0.8);
			id++;
		}
		
		for (double i = -0.2; i <= 0.9; i += 0.1)
		{
			models_manager->AddQuad(i, -0.8, i + 0.1, -0.9, id, path3);
			cornerList.AddCorner(i, -0.8, i + 0.1, -0.9);
			id++;
		}
		//----
		for (double i = -1.; i <= -0.7; i += 0.1)
		{
			models_manager->AddQuad(i, -0.8, i + 0.1, -0.9, id, path1);
			cornerList.AddCorner(i, -0.8, i + 0.1, -0.9);
			id++;
		}
		
		for (double i = -0.8; i <= 0.; i += 0.1)
		{
			models_manager->AddQuad(i, -0.3, i + 0.1, -0.4, id, path1);
			cornerList.AddCorner(i, -0.3, i + 0.1, -0.4);
			id++;
		}
		for (double i = 0.5; i <= 1.; i += 0.1)
		{
			models_manager->AddQuad(i, -0.4, i + 0.1, -0.5, id, path1);
			cornerList.AddCorner(i, -0.4, i + 0.1, -0.5);
			id++;
		}
		for (double i = 0.; i <= 0.4; i += 0.1)
		{
			models_manager->AddQuad(i, 0., i + 0.1, -0.1, id, path1);
			cornerList.AddCorner(i, 0., i + 0.1, -0.1);
			id++;
		}
		for (double i = 0.7; i <= 1.; i += 0.1)
		{
			models_manager->AddQuad(i, -0.3, i + 0.1, -0.4, id, path1);
			cornerList.AddCorner(i, -0.3, i + 0.1, -0.4);
			id++;
		}
		
		models_manager->AddQuad(-0.2, 0.3,  -0.1, 0.2, id, path1);
		cornerList.AddCorner(-0.2, 0.3,  -0.1, 0.2);
		id++;
		models_manager->AddQuad(-0.5, 0.5, -0.4, 0.4, id, path1);
		cornerList.AddCorner(-0.5, 0.5, -0.4, 0.4);
		id++;
	//------vertical
		
		for (double i = -0.3; i <= 0.35; i += 0.1)
		{
			models_manager->AddQuad(-0.5, i + 0.1 , -0.4, i, id, path3);
				cornerList.AddCorner(-0.5, i + 0.1 , -0.4, i);
			id++;
		}
		
			
		
		for (double i = -0.3; i <= -0.1; i += 0.1)
		{
			models_manager->AddQuad(0., i + 0.1, 0.1, i, id, path3);
			cornerList.AddCorner(0., i + 0.1, 0.1, i);
			id++;
		}
		
		/*models_manager->AddQuad(-0.5, -0.2, -0.4, -0.3, id, path1);
		cornerList.AddCorner(-0.5, -0.2, -0.4, -0.3);
		id++;*/
		
		//for (double i = 0.3; i <= 0.7; i += 0.1)
		//{
		//	models_manager->AddQuad(-0.4, i + 0.1, -0.5, i, id, path1);
		//	cornerList.AddCorner(-0.4, i + 0.1, -0.5, i);
		//	id++;
		//}
		
		
		//Death
		for (double i = -0.5; i <= -0.1; i += 0.1)
		{
			models_manager->AddQuad(i, -0.2, i + 0.1, -0.3, id, pathDeath);
			cornerList.AddCorner(i, -0.2, i + 0.1, -0.3,DEATH);
			id++;
		}
		for (double i = 0.3; i <= 0.5; i += 0.1)
		{
			models_manager->AddQuad(i, -0.7, i + 0.1, -0.8, id, pathDeath);
			cornerList.AddCorner(i, -0.7, i + 0.1, -0.8, DEATH);
			id++;
		}
		
		//Finish
		for (double i = -1.; i <= -0.8; i += 0.1)
		{
			models_manager->AddQuad(i, -0.3, i + 0.1, -0.4, id, pathFinish);
			cornerList.AddCorner(i, -0.3, i + 0.1, -0.4, FINISH);
			id++;
		}
		
		
		models_manager->AddQuadMove(-0.8, -0.5, -0.7, -0.7, id, anim/*path2*/, cornerList,net);//staticObj);
		//if (net.connType != INVALID)
		//{
		//	id++;
		//	models_manager->AddQuadNetwork(-0.8, -0.5, -0.7, -0.7, id, anim, cornerList,net);
		//}
		//char s[10];
		//sprintf_s(s, "%d", id);

	}
	else
	{
		//--------Multiplayer

		for (double i = -1.; i <= 0.9; i += 0.1)
		{
			models_manager->AddQuad(i, -0.9, i + 0.1, -1., id, path1);
			//staticObj.push_back(Corners(i , -0.9, i + 0.1, -1.));
			cornerList.AddCorner(i, -0.9, i + 0.1, -1.);
			id++;
		}


		//Left


		for (double i = -0.9; i <= -0.8; i += 0.1)
		{
			models_manager->AddQuad(i, -0.4, i + 0.1, -0.5, id, path1);
			cornerList.AddCorner(i, -0.4, i + 0.1, -0.5);
			id++;
		}

		for (double i = -0.5; i <= -0.2; i += 0.1)
		{
			models_manager->AddQuad(i, -0.6, i + 0.1, -0.7, id, path1);
			cornerList.AddCorner(i, -0.6, i + 0.1, -0.7);
			id++;
		}

		for (double i = -0.4; i <= -0.2; i += 0.1)
		{
			models_manager->AddQuad(i, -0.1, i + 0.1, -0.2, id, path1);
			cornerList.AddCorner(i, -0.1, i + 0.1, -0.2);
			id++;
		}

		models_manager->AddQuad(-0.2, 0.2, -0.1, 0.1, id, path1);
		cornerList.AddCorner(-0.2, 0.2, -0.1, 0.1);
		id++;



		//right

		for (double i = 0.7; i <= 0.8; i += 0.1)
		{
			models_manager->AddQuad(i, -0.4, i + 0.1, -0.5, id, path1);
			cornerList.AddCorner(i, -0.4, i + 0.1, -0.5);
			id++;
		}
		for (double i = 0.1; i <= 0.4; i += 0.1)
		{
			models_manager->AddQuad(i, -0.6, i + 0.1, -0.7, id, path1);
			cornerList.AddCorner(i, -0.6, i + 0.1, -0.7);
			id++;
		}
		for (double i = 0.1; i <= 0.35; i += 0.1)
		{
			models_manager->AddQuad(i, -0.1, i + 0.1, -0.2, id, path1);
			cornerList.AddCorner(i, -0.1, i + 0.1, -0.2);
			id++;
		}

		models_manager->AddQuad(0.1, 0.2, 0.2, 0.1, id, path1);
		cornerList.AddCorner(0.1, 0.2, 0.2, 0.1);
		id++;

		//------vertical

		for (double i = -0.9; i <= 0.2; i += 0.1)
		{
			models_manager->AddQuad(-0.1, i + 0.1, 0., i, id, path3);
			cornerList.AddCorner(-0.1, i + 0.1, 0., i);
			id++;
		}
		for (double i = -0.9; i <= 0.2; i += 0.1)
		{
			models_manager->AddQuad(0., i + 0.1, 0.1, i, id, path3);
			cornerList.AddCorner(0., i + 0.1, 0.1, i);
			id++;
		}

		for (double i = -0.9; i <= 0.9; i += 0.1)
		{
			models_manager->AddQuad(-1., i + 0.1, -0.9, i, id, path3);
			cornerList.AddCorner(-1., i + 0.1, -0.9, i);
			id++;
		}

		for (double i = -0.9; i <= 0.9; i += 0.1)
		{
			models_manager->AddQuad(0.9, i + 0.1, 1., i, id, path3);
			cornerList.AddCorner(0.9, i + 0.1, 1., i);
			id++;
		}

		for (double i = -0.1; i <= 0.; i += 0.1)
		{
			models_manager->AddQuad(i, 0.4, i + 0.1, 0.3, id, pathFinish);
			cornerList.AddCorner(i, 0.4, i + 0.1, 0.3, FINISH);
			id++;
		}

		if (net.connType == CLIENT)
		{
			models_manager->AddQuadMove(0.7, -0.5, 0.8, -0.7, id, anim/*path2*/, cornerList, net);//staticObj);
			if (net.connType != INVALID)
			{
				id++;
				models_manager->AddQuadNetwork(-0.8, -0.5, -0.7, -0.7, id, anim, cornerList, net);
			}
			char s[10];
			sprintf_s(s, "%d", id);
		}
		else
		{
			models_manager->AddQuadMove(-0.8, -0.5, -0.7, -0.7, id, anim/*path2*/, cornerList, net);//staticObj);
			if (net.connType != INVALID)
			{
				id++;
				models_manager->AddQuadNetwork(0.7, -0.5, 0.8, -0.7, id, anim, cornerList, net);
			}
			char s[10];
			sprintf_s(s, "%d", id);
		}

	}



	models_manager->Update();
	

	//Rendering::Models::Quad a = (Rendering::Models::Quad) models_manager->GetModel(s);
}

Scene_Manager::~Scene_Manager()
{
	delete shader_manager;
	delete models_manager;
}

void Scene_Manager::notifyBeginFrame()
{
	models_manager->Update();
}

void Scene_Manager::notifyDisplayFrame()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.6, 0.85, 0.91, 1.0);

	models_manager->Draw();

}

void Scene_Manager::notifyEndFrame()
{
	//nothing here for the moment
}

void Scene_Manager::notifyReshape(int width,
	int height,
	int previous_width,
	int previous_height)
{
	//nothing here for the moment 

}