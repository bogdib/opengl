#pragma once
#include "Model.h"
namespace Rendering
{
	namespace Models
	{
		class Quad : public Model
		{
		public:
			double speed=0;

			Quad();
			~Quad();


			void Create(double xul, double yul, double xlr, double ylr);
			virtual void Draw() override final;
			virtual void Update() override final;
		};
	}
}