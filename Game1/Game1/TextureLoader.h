#pragma once
#include <GL\glew.h>
#include <fstream>
#include <iostream>
#include <string>
#include "BMPHeader.h"


namespace Rendering
{
	namespace Texture
	{
		class TextureLoader
		{
		public:
			TextureLoader();
			~TextureLoader();

			static unsigned int LoadTexture(const std::string& filename,unsigned int width,unsigned int height);
		private:

			static void LoadBMPFile(const std::string& filename,
				unsigned int& width,
				unsigned int& height,
				unsigned char*& data);
		};
	}
}
