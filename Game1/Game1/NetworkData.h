#pragma once
#include <string.h>
#define MAX_PACKET_SIZE 1000000

enum PacketTypes 
{
	INIT_CONNECTION = 0,
	ACTION_EVENT = 1,
};

struct Packet
{

	unsigned int packet_type;
	

	void serialize(char * data,float ox, float oy, float tNum) 
	{
		float a[3] = {ox,oy,tNum};
		memcpy(data, a, 3 * sizeof(float));
	}

	void deserialize(char * data, float &ox, float &oy, float &tNum)
	{
		packet_type = 1;
		float a[3];
		memcpy(a, data, 3 * sizeof(float));
		ox = a[0]; oy = a[1]; tNum = a[2];
		puts("");
	}
};

//static struct Data
//{
//public:
//	static float ox, oy, tNum;
//	static void setData(float a, float b, float c)
//	{
//		Data::ox = a;
//		Data::oy = b;
//		Data::tNum = c;
//	}
//};
//float Data::ox = 0;
//float gox=0;