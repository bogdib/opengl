#pragma once
#include "Init_GLUT.h"
#include "Scene_Manager.h"
#include "ServerGame.h"
#include "ClientGame.h"
#include <process.h>
#include "Network_Manager.h"
ServerGame * server;
ClientGame * client;

void serverLoop(void * arg)
{
	std::cout << "IN SERVER";
	while (true)
	{
		server->update();
	}
}

void clientLoop(void * arg)
{
	while (true)
	{
		client->update();
	}
}


using namespace Core;
using namespace Init;
int main(int argc, char **argv)
{
	WindowInfo window(std::string("Game"),100, 0, 800, 600, true);
	ContextInfo context(4, 5, true);
	FramebufferInfo frameBufferInfo(true, true, true, true);
	Init::Init_GLUT::init(window, context, frameBufferInfo);

	
	char type[10] = "";
	if (argc > 1 && !strcmp(argv[1], "server"))
	{
		server = new ServerGame();
		_beginthread(serverLoop, 0, (void*)12);
		strcpy_s(type,"server");

	}
	if (argc > 1 && !strcmp(argv[1], "client"))
	{
		client = new ClientGame();
		_beginthread(clientLoop, 0, (void*)12);
		strcpy_s(type,"client");
	}
	
	Network_Manager net(type,server,client);

	IListener* scene = new Managers::Scene_Manager(net);
	Init::Init_GLUT::setListener(scene);

	Init::Init_GLUT::run();

	delete scene;
	return 0;
}

//#pragma once
//#include <Windows.h>
//#include <GL\glew.h>
//#include <GL\freeglut.h>
//#include <iostream>
//#include "Shader_Loader.h"
//#include "GameModels.h"
//#include "Shader_Manager.h"


//using namespace Core;
//GLuint program;
//Models::GameModels *gameModels;
//Managers::Shader_Manager* shaderManager;
//
//void Init()
//{
//
//	glEnable(GL_DEPTH_TEST);
//
//	gameModels = new Models::GameModels();
//	gameModels->CreateTriangleModel("triangle1");
//	//load and compile shaders
//	shaderManager = new Managers::Shader_Manager(); //thanks to Erik
//													// for pointing this out
//	shaderManager->CreateProgram("colorShader",
//		"Vertex_Shader.glsl",
//		"Fragment_Shader.glsl");
//	program = Managers::Shader_Manager::GetShader("colorShader");
//
//}
//
//void renderScene(void)
//{
//
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//	glClearColor(1.0, 0.0, 0.0, 1.0);//clear red
//
//	//uses the created program
//	glUseProgram(program);
//
//	glBindVertexArray(gameModels->GetModel("triangle1"));
//	//draw 3 vertices as triangles
//	glDrawArrays(GL_TRIANGLES, 0, 3);
//	//glDrawArrays(GL_TRIANGLES, 3, 3);
//
//	glutSwapBuffers();
//}
//
//void closeCallback()
//{
//
//	std::cout << "GLUT:\t Finished" << std::endl;
//	glutLeaveMainLoop();
//}
//
////don't forget to delete the manager in main
//int main(int argc, char **argv)
//{
//	glutInit(&argc, argv);
//	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
//	//glutInitWindowPosition(500, 500);//optional
//	glutInitWindowSize(800, 600); //optional
//	glutCreateWindow("OpenGL Window");
//	glewInit();
//	
//	if (glewIsSupported("GL_VERSION_4_5")) //lower your version if 4.5 is not supported by your video card
//	{
//		std::cout << " OpenGL Version is 4.5\n ";
//	}
//	else
//	{
//		std::cout << "OpenGL 4.5 not supported\n ";
//	}
//	
//	Init();
//	//renderScene();
//	glutDisplayFunc(renderScene);
//	glutCloseFunc(closeCallback);
//
//	glutMainLoop();
//
//	delete gameModels;
//	delete shaderManager;
//	return 0;
//}


//using namespace Core;
//
//GLuint program;
//Models::GameModels *gameModels;
//
//
//void renderScene(void)
//{
//
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//	glClearColor(1.0, 0.0, 0.0, 1.0);//clear red
//
//	//uses the created program
//	glUseProgram(program);
//
//	glBindVertexArray(gameModels->GetModel("triangle1"));
//	//draw 3 vertices as triangles
//	glDrawArrays(GL_TRIANGLES, 0, 3);
//	//glDrawArrays(GL_TRIANGLES, 3, 3);
//
//	glutSwapBuffers();
//}
//
//
//void closeCallback()
//{
//
//	std::cout << "GLUT:\t Finished" << std::endl;
//	glutLeaveMainLoop();
//}
//
//void Init()
//{
//
//	glEnable(GL_DEPTH_TEST);
//
//	gameModels = new Models::GameModels();
//	gameModels->CreateTriangleModel("triangle1");
//
//	//load and compile shaders
//	Shader_Loader shaderLoader;
//	program = shaderLoader.CreateProgram("Vertex_Shader.glsl",
//		"Fragment_Shader.glsl");
//	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//}
//
//
//int main(int argc, char **argv)
//{
//
//	glutInit(&argc, argv);
//	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
//	//glutInitWindowPosition(500, 500);//optional
//	glutInitWindowSize(800, 600); //optional
//	glutCreateWindow("OpenGL Window");
//	glewInit();
//
//	if (glewIsSupported("GL_VERSION_4_5")) //lower your version if 4.5 is not supported by your video card
//	{
//		std::cout << " OpenGL Version is 4.5\n ";
//	}
//	else
//	{
//		std::cout << "OpenGL 4.5 not supported\n ";
//	}
//	Init();
//	// register callbacks
//	glutDisplayFunc(renderScene);
//	glutCloseFunc(closeCallback);
//
//	glutMainLoop();
//
//	delete gameModels;
//	glDeleteProgram(program);
//
//	return 0;
//}
